package com.github.pamugk.polyhymniamusicplayer.ui.screens.main

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@Composable
internal fun ArtistsFragment(padding: PaddingValues = PaddingValues()) {
    LazyColumn(modifier = Modifier.padding(padding)) {

    }
}